package generate;

import io.bloco.faker.Faker;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GenerateBigNestedJSONsForMongo {
    static FileWriter file;
    static int NO_OF_FILES = 1;
    static int NO_OF_COMPANIES = 5;
    static int NO_OF_EMPLOYEES = 10;

    public static void main(String[] args) {
        try {
            String filePath = ".";
            if (args.length == 4) {
                NO_OF_COMPANIES = Integer.parseInt(args[0]);
                NO_OF_EMPLOYEES = Integer.parseInt(args[1]);
                NO_OF_FILES = Integer.parseInt(args[2]);
                filePath = args[3];
            }

            for (int k = 1; k <= NO_OF_FILES; k++) {
                File f = new File(filePath + File.separator + "json_doc_" + k + "_" + System.currentTimeMillis() + ".json");
                System.out.println("File #" + k + " generated at "+f.getCanonicalPath());
                file = new FileWriter(f);
                Faker faker = new Faker();

                file.write("[");
                List<JSONObject> mlist = new ArrayList<>();
                for (int i = 0; i < NO_OF_COMPANIES; i++) {
                    JSONObject bigJson = new JSONObject();
                    bigJson.put("company_ceo", faker.app.author());
                    bigJson.put("company_ccno", faker.business.creditCardNumber());
                    bigJson.put("company_email", faker.internet.email());

                    List<JSONObject> list = new ArrayList<>();
                    for (int m = 0; m < NO_OF_EMPLOYEES; m++) {
                        JSONObject obj = new JSONObject();
                        obj.put("emp_name", faker.name.firstName());
                        obj.put("emp_email", faker.internet.email());
                        obj.put("emp_ip", faker.internet.ipV4Address());
                        obj.put("emp_address", faker.address.streetAddress());
                        obj.put("emp_salary", faker.number.between(10000, 99000));

                        JSONObject objK = new JSONObject();
                        objK.put("emp_username", faker.internet.userName());
                        objK.put("emp_userdomain", faker.internet.domainName());
                        objK.put("emp_website", faker.internet.url());

                        JSONObject objN = new JSONObject();
                        objN.put("emp_father", faker.app.author());
                        objN.put("emp_ccno", faker.business.creditCardNumber());
                        objN.put("emp_semail", faker.internet.email());
                        objK.put("emp_personal_details", objN);

                        obj.put("emp_other_details", objK);

                        list.add(obj);
                    }
                    bigJson.put("emp_details", list);
                    file.write(bigJson.toJSONString());
                    if (i != NO_OF_COMPANIES - 1)
                        file.write(",");
                }
                file.write("]");

                file.flush();
                file.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}