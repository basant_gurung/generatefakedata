package generate;

import io.bloco.faker.Faker;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

// Generate Simple Jsons for Mongo and Cassandra
public class GenerateJSONsForMongo {
    static FileWriter file;
    static int n = 1000;

    public static void main(String[] args) {
        try {
            for (int k = 0; k < 1; k++) {
                System.out.println("File #" + (k + 1));
                file = new FileWriter("C:\\data\\simple\\json_doc_" + k + "_" + System.currentTimeMillis() + ".json"); //try opening the file

                file.write("[");
                for (int i = 0; i < n; i++) {
                    Faker faker = new Faker();
                    JSONObject obj = new JSONObject();
                    String name = faker.name.firstName().replace("'", "");
                    String email = faker.internet.email();
                    String ip = faker.internet.ipV4Address();
                    String address = faker.address.streetAddress().replace("'", "");
                    int salary = faker.number.between(10000, 99000);
                    obj.put("name", name);
                    obj.put("email", email);
                    obj.put("ip", ip);
                    obj.put("address", address);
                    obj.put("salary", salary);
                    obj.put("ccno", faker.business.creditCardNumber());
                    obj.put("contact_no", faker.phoneNumber.cellPhone());
                    file.write(obj.toJSONString());
                    if (i != n - 1)
                        file.write(",");
                }
                file.write("]");
                file.flush();
                file.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}