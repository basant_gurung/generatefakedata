package generate;

import io.bloco.faker.Faker;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

// Generate Simple Jsons for Mongo and Cassandra
public class GenerateJSONsForMongoCassandra {
    static FileWriter file, file1;
    static int n = 10000;

    public static void main(String[] args) {
        try {
            int id = 1;
            for (int k = 1; k < 6; k++) {
                System.out.println("File #" + (k + 1));
                file = new FileWriter("C:\\data\\cassandra\\json_doc_" + k + "_" + System.currentTimeMillis() + ".json"); //try opening the file
                file1 = new FileWriter("C:\\data\\cassandra\\cassandra_doc_" + k + "_" + System.currentTimeMillis() + ".sql"); //try opening the file

                file.write("[");
                for (int i = 0; i < n; i++) {
                    id++;
                    Faker faker = new Faker();
                    JSONObject obj = new JSONObject();
                    String name = faker.name.firstName().replace("'", "");
                    String email = faker.internet.email();
                    String ip = faker.internet.ipV4Address();
                    String address = faker.address.streetAddress().replace("'", "");
                    int salary = faker.number.between(10000, 99000);
                    obj.put("name", name);
                    obj.put("email", email);
                    obj.put("ip", ip);
                    obj.put("address", address);
                    obj.put("salary", salary);
                    file.write(obj.toJSONString());
                    if (i != n - 1)
                        file.write(",");

                    String val = "insert into basant_db.large_table(id, name, email, ip, address, salary) values (";

                    val = val.concat(String.valueOf(id)).concat(",'").concat(name).concat("','").concat(email).concat("','").concat(ip).concat("','").concat(address).concat("',").concat(String.valueOf(salary));

                    val = val.concat(");");

                    file1.write(val + "\n");
                }
                file.write("]");
                file.flush();
                file.close();

                file1.flush();
                file1.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}