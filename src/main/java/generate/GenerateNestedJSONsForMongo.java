package generate;

import io.bloco.faker.Faker;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

public class GenerateNestedJSONsForMongo {
    static FileWriter file;
    static int n = 100000;

    public static void main(String[] args) {
        try {
            for (int k = 0; k < 4; k++) {
                System.out.println("File #"+(k+1));
                file = new FileWriter("C:\\data\\nested\\json_doc_" + n + "_" + System.currentTimeMillis() + ".json"); //try opening the file
                file.write("[");
                for (int i = 0; i < n; i++) {
                    Faker faker = new Faker();
                    JSONObject obj = new JSONObject();
                    obj.put("name", faker.name.firstName());
                    obj.put("email", faker.internet.email());
                    obj.put("ip", faker.internet.ipV4Address());
                    obj.put("address", faker.address.streetAddress());
                    obj.put("salary", faker.number.between(10000, 99000));

                    JSONObject objN2 = new JSONObject();
                    objN2.put("ca_name", faker.app.author());
                    objN2.put("ca_ccno", faker.business.creditCardNumber());
                    objN2.put("ca_email", faker.internet.email());

                    JSONObject objN = new JSONObject();
                    objN.put("company_ceo", faker.app.author());
                    objN.put("company_ccno", faker.business.creditCardNumber());
                    objN.put("company_email", faker.internet.email());
                    objN.put("ca_audit", objN2);

                    obj.put("company", objN);
                    file.write(obj.toJSONString());
                    if (i != n - 1)
                        file.write(",");
                }
                file.write("]");
                file.flush();
                file.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}